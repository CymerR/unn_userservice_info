#include <iostream>
using namespace std;

// Strategy interface
class Strategy {
public:
    virtual void execute() = 0;
};

// Concrete strategies
class ConcreteStrategyA : public Strategy {
public:
    void execute() {
        cout << "Executing strategy A" << endl;
    }
};

class ConcreteStrategyB : public Strategy {
public:
    void execute() {
        cout << "Executing strategy B" << endl;
    }
};

// Context class
class Context {
private:
    Strategy* strategy;

public:
    void setStrategy(Strategy* strategy) {
        this->strategy = strategy;
    }

    void executeStrategy() {
        strategy->execute();
    }
};

int main() {
    // Create the context object
    Context context;

    // Set the concrete strategy A
    ConcreteStrategyA strategyA;
    context.setStrategy(&strategyA);
    context.executeStrategy(); // Output: Executing strategy A

    // Set the concrete strategy B
    ConcreteStrategyB strategyB;
    context.setStrategy(&strategyB);
    context.executeStrategy(); // Output: Executing strategy B

    return 0;
}