#include "imports.h"
#include <fstream>
#include <istream>
#include <map>
#include <ostream>
#include <string>
#include <vector>

using namespace std;

class Encoder {
public:
  virtual string encode(string &input) = 0;
  virtual ~Encoder() {}
};

class RotEncoder : public Encoder {
  int shift;

public:
  RotEncoder(int shift) : shift(shift) {}
  string encode(string &input) {
    auto output = new char[input.size() + 1];
    for (int i = 0; i < input.size(); i++) {
      output[i] = (input[i] + shift - 'a') % 26 + 'a';
    }
    return string(output);
  }
};

class AtbEncoder : public Encoder {

public:
  string encode(string &input) {
    char *output = new char[input.size() + 1];
    for (int i = 0; i < input.size(); i++) {
      output[i] = -(input[i] - 'a') + 25 + 'a';
    }
    return string(output);
  }
};

class DummyEncoder : public Encoder {
public:
  string encode(string &input) { return RotEncoder(26).encode(input); }
};

auto configure_type_function(int n) {

  return [n](string &type) -> Encoder * {
    if (type == "rot") {
      return new RotEncoder(n);
    } else if (type == "atb") {
      return new AtbEncoder();
    } else {
      return new DummyEncoder();
    }
  };
}

auto get_by_type = configure_type_function(13);

#pragma region abstract

class EncoderService {
private:
  Encoder *encoder;

public:
  EncoderService(Encoder *encoder) : encoder(encoder) {}
  string encode(string &input) { return encoder->encode(input); }
  ~EncoderService() { delete encoder; }
};

class EncoderServiceFactory {
private:
  string type;

public:
  EncoderServiceFactory(string t) : type(t) {}

  EncoderService *getEncoderService() {
    return new EncoderService(get_by_type(type));
  }
};

class User {
private:
  string name;
  string password;

public:
  User(string name, string password) : name(name), password(password) {}
  string getName() { return name; }
  string getPassword() { return password; }
  operator string() { return name + " : " + password; }
  // operator for cout
  friend ostream &operator<<(ostream &os, const User &user) {
    return os << user.name << " : " << user.password;
  }

  int size() { return password.size(); }
};

class UserService {
private:
  EncoderService *encoderService;

public:
  UserService(EncoderService *encoderService)
      : encoderService(encoderService) {}
  User login(string name, string password) {
    return User(name, encoderService->encode(password));
  }
};

class UserServiceFactory {
private:
  EncoderService *encoderService;

public:
  UserServiceFactory(EncoderService *encoderService)
      : encoderService(encoderService) {}

  UserService *getUserService() { return new UserService(encoderService); }
};

enum InputType { Console, File };

class Input {
public:
  virtual istream &read() = 0;
  virtual ~Input() {}
};

class ConsoleInput : public Input {
public:
  istream &read() { return cin; }
};

class FileInput : public Input {
private:
  ifstream file;

public:
  FileInput(string filename) { file = ifstream(filename); }
  istream &read() { return file; }
};

class Controller {
private:
  UserService *userService;
  Input *input;

  template <typename T> T maxLength(vector<T> vec) {
    T max = vec[0];
    for (auto item : vec) {
      if (item.size() > max.size()) {
        max = item;
      }
    }
    return max;
  }

public:
  Controller(UserService *userService, Input *input)
      : userService(userService), input(input) {}
  void handle() {
    vector<User> userList;

    string name;
    string password;
    while (input->read() >> name >> password) {

      if (name == "q") {
        break;
      }
      auto user = userService->login(name, password);
      userList.push_back(user);
    }

    for (auto user : userList) {
      cout << user << endl;
    }
    cout << "Max password length user: " << maxLength(userList) << endl;
  }
};

class ApplicationContext {
private:
  UserService *userService;
  EncoderService *encoderService;
  Input *input;

public:
  ApplicationContext(string type, InputType input_type) {
    switch (input_type) {
    case Console:
      input = new ConsoleInput();
      break;
    case File:
      input = new FileInput("input.txt");
      break;
    default:
      input = new ConsoleInput();
      break;
    }
    encoderService = EncoderServiceFactory(type).getEncoderService();
    userService = UserServiceFactory(encoderService).getUserService();
  }
  void run() {
    Controller controller(userService, input);
    controller.handle();
  }
  ~ApplicationContext() {
    delete userService;
    delete encoderService;
    delete input;
  }
};

#pragma endregion
int main() {
  ApplicationContext app("rot", File);
  app.run();
  return 0;
}
